% Property of IRsweep AG 
% Evaluation_ProcessedData
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

%%% To use this script, add the its folder to the path of matlab(with subfolder)
%%% and copy this script somewhere else so that you can modify it to fit
%%% your needs.

%% Clear all variable and plots
clear all;
close all;

%% Specify the folder and the name of the file. Folder should end by a backslash
dataPath = 'XXXXXXXXXXXXXXX\XXXXXXXXXXXXX\';
measName = 'XXXXXXXXXXX_processed_data.h5';

measType = h5getTypeOfMeasurement(dataPath,measName);


%% Load data from the processed hdf5 file
if contains(measType,'TimeResolved')
    [transientTrans,timeAxis,~,evalParams] = readTimeResolvedProcessedData(dataPath, measName);
else
    [transmission,~,evalParams] = readStaticProcessedData(dataPath, measName);
end


%% Average the transient over files. 
%This will create a new .mat file with the corresponding data inside 

startFileNum=-1; % put -1 if you want to average all processed data
stopFileNum=3;

if contains(measType,'TimeResolved')
    [avgTransientTrans, ~, evalParams] = averageTransientAcquisitions(evalParams, startFileNum, stopFileNum, true);
else
    [avgTransmission, evalParams] = averageStaticTransmissionAcquisitions(evalParams, startFileNum, stopFileNum, true);
end


%% Calculate the standard deviation of measurements as function of peak number before smoothing
[stdPeak, evalParams] = stdVsPeakNumber(evalParams, true);

%% Spectral smoothing of tranmsission data and standard deviation of measurement
specAvgHalfWidth = 5;
[avgTransmission, evalParams] = spectralAverageGlobal(evalParams, specAvgHalfWidth, true);
[stdPeak, evalParams] = stdVsPeakNumber(evalParams, true);

%% Specify the path of the calibration measurement : 
% calibMeasFolder = folder where the static measurement of IRisF1 is stored
calibMeasFolder = 'XXXXXX\XXXXXX\';
% calibMeasName = name of the file of the static measurement of IRisF1
calibMeasName = 'XXXXXXXXXXXXXXXX_processed_data.h5';
% calibReferencePath = full name of the reference file 
calibReferencePath = 'XXXXXXXXXXXXX\XXXXXXXXXXXX\XXXXXXXXXXX.mat';

evalParams = addCalibEvaluationParams(evalParams, calibMeasFolder, calibMeasName, calibReferencePath);

%% Process calibration data. If already processed once, it does not have to be done again (skip this section).
[~,wn,evalParamsCalib] = readStaticProcessedData(calibMeasFolder, calibMeasName);

%% Average the transmission calibration
[~, evalParamsCalib] = averageStaticTransmissionAcquisitions(evalParamsCalib, -1, stopFileNum, true);
[~, evalParamsCalib] = spectralAverageGlobal(evalParamsCalib, specAvgHalfWidth, true);


%% Generate the wavenumber axis that corresponds to the data
[ wnAxis, evalParams] = makeWnAxisGlobal(evalParams,evalParamsCalib, true);

%% At the point, the processing part is finished.
%%% You can change the rest of the script to according to your wishes in 
%%% -> time resolution
%%% -> spectral region
%%% -> thresholding of the peak to hide noisy points
%%% -> time region

labelname = 'Unkown measurement';

%% Get transient in a given wavenumber range

timeRes = 5e-5;
noTimeSteps = 250;

fig_obj=figure;
xlabel('Time s');
ylabel('\Delta Absorbance');

minWn = 1199;
maxWn = 1200;
[fig_obj,transient, evalParams] = plotTransientInWnRange(fig_obj,evalParams,minWn, maxWn,timeRes,labelname);



minWn = 1220;
maxWn = 12250;
[fig_obj,transient, evalParams] = plotTransientInWnRange(fig_obj,evalParams,minWn, maxWn,timeRes,labelname);


%%
[realTransients, timeAxis, evalParams] = generateRealOutput(evalParams, 'absorbance', true);
%% Get spectrum in a given time range and plot it
thresholdCommon = 0.8;

fig_obj=figure;
xlabel('Wavenumber cm^{-1}');
ylabel('\Delta Absorbance');

startTime = 2.5e-3;
stopTime = 4.5e-3;
[fig_obj,SpecA,evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,labelname);

startTime = 0.259e-3;
stopTime = 0.334e-3;
[fig_obj,SpecB,evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,labelname);

startTime = 0.334e-3;
stopTime = 0.543e-3;
[fig_obj,SpecC,evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,labelname);

startTime = 0.543e-3;
stopTime = 1.043e-3;
[fig_obj,SpecD,evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,labelname);

startTime = 1.043e-3;
stopTime = 10.243e-3;
[fig_obj,SpecE,evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,labelname);
