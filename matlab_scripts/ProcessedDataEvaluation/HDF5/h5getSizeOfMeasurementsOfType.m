% Property of IRsweep AG 
% h5getSizeOfMeasurementsOfType
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [binSize, sampSize] = h5getSizeOfMeasurementsOfType(folder, filename, measType)
% This function retrieves the sample size of acquisitions we have of a 
% particular type in an hdf5 file 
% Inputs:
% - folder: folder where to find the file
% - filename: filename of the file
% - measType: string containing the name of the measurement type
% Outputs:
% - binSize: binary logarithm of sample size
% - size: sample size

info = h5info(strcat(folder,filename));
groupInd = h5findGroupIndexWithName(folder, filename, measType);
sampSize = info.Groups(groupInd).Groups(1).Datasets(1).Dataspace.Size;
binSize = round(log2(sampSize));