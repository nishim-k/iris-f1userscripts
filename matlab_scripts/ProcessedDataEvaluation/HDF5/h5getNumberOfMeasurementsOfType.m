% Property of IRsweep AG 
% h5getNumberOfMeasurementsOfType
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [noMeas] = h5getNumberOfMeasurementsOfType(folder, filename, measType)
% This function retrieves the number of acquisitions we have of a 
% particular type in an hdf5 file 
% Inputs:
% - folder: folder where to find the file
% - filename: filename of the file
% - measType: string containing the name of the measurement type
% Outputs:
% - noMeas: number of measurements of that type

info = h5info(strcat(folder,filename));
groupInd = h5findGroupIndexWithName(folder, filename, measType);
noMeas = size(info.Groups(groupInd).Groups,1);