% Property of IRsweep AG 
% readTimeResolvedProcessedData
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRsweep. All rights reserved.

function [transientTrans,timeAxis,wnAxis,evalParams] = readTimeResolvedProcessedData(dataPath, measName)
% This function loads the data of a time resolved measurement saved in a 
% hdf5 file
% Input:
% - dataPath: Folder where measurements are saved
% - measName: body of the filename (up to underscore before channel
% number.
% Output:
% - transientTrans: matrix containing transient transmissions.
% - timeAxis: vector with time axis.
% - wnAxis: vector with wavenumber axis.
% - evalParams: structure containing the evaluation parameters.

%% Load evaluation parameters
evalParams = makeEvaluationParamStructFromProcessedH5(dataPath, measName);

%% Load time and wavenumber axis
fprintf('Loading time axis : ')
timeAxis = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Groups(1).Name,'/time'));
fprintf('Done\n')
fprintf('Loading wavenumber axis : ')
wnAxis = h5read(strcat(dataPath,measName),strcat(evalParams.infogroup.Name,'/first_wn_axis'));
fprintf('Done\n')


%% Load standard deviation of peaks

fprintf('Loading of the deviation of peaks : ')
stdPeak = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Name,'/info/peakstd'));
fprintf('Done\n')
    

%% Load peakMeanAmp
fprintf('Loading average peak amplitudes : ')
peakMeanAmp = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Groups(1).Name,'/peakmeanamp'));
fprintf('Done\n')

%% Load transmission data
numOfFile = h5findNumAcquisition(evalParams.dataGroup);
transientTrans = zeros(size(timeAxis,1),size(wnAxis,1),numOfFile);
fprintf('Loading transients : ')
for ii=1:numOfFile
    tmp_amp = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Groups(ii).Name,'/amp'));
    transientTrans(:,:,ii)=tmp_amp(1,:,:)+tmp_amp(2,:,:).*1i;
end
fprintf('Done\n')

%% Save to .mat file
fprintf('Saving data in mat file : ')
evalParams.processedName = strcat(erase(evalParams.processedName, '.h5'), '.mat');
evalParams.dataVariables = [{'transientTrans'},{'timeAxis'},{'peakMeanAmp'},{'wnAxis'},{'evalParams'}];

if exist('stdPeak','var')
    evalParams.dataVariables{end+1}='stdPeak';
end

save(evalParams.processedName, evalParams.dataVariables{1});
for ii = 2:size(evalParams.dataVariables,2)
    save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
end
fprintf('Done\n\n')
