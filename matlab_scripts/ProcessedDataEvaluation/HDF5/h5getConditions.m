% Property of IRsweep AG 
% h5getConditions
% Created by Quentin Saudan on 30/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [Current1, Current2,Temperature1, Temperature2] = h5getConditions(group, fileNum)
% This function read the hdf5 and returns the operating conditions for a
% specific group.
% Input:
% - group
% - fileNum: number of the measurement file.

timegroup = h5findGroup(group,'time_domain');

%% Check if the acquisition are directly in the top one and it's not empty
if ~isempty(h5findGroup(group,'acquisition')) && size(group.Groups,1)>=fileNum
    final_group = group;
%Check if the acquisition are save in a sub group name time_domain in the
%group 
elseif ~isempty(timegroup) && ~isempty(h5findGroup(timegroup,'acquisition')) && size(timegroup.Groups,1)>=fileNum
    final_group = timegroup;
    
%Otherwise, throw an error    
else
    error('No acquisition found to take the conditions')
end   

%% Now let's extract the metadata from the file : 

infogroup = h5findGroup(final_group.Groups(fileNum),'/info');

Current1 = get_attribute_from_group(infogroup,'Laser1Current','(in A)');
Current2 = get_attribute_from_group(infogroup,'Laser2Current','(in A)');
Temperature1 = get_attribute_from_group(infogroup,'Laser1Temperature','(?C)');
Temperature2 = get_attribute_from_group(infogroup,'Laser2Temperature','(?C)');


end

