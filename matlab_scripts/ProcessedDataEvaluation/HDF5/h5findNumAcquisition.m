% Property of IRsweep AG 
% h5findGroup
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [counter] = h5findNumAcquisition(parentGroup)
% This function looks for a group name in an hdf5 file and returns the
% index of the group.
% Inputs:
% - folder: folder where to find the file
% - filename: filename of the file
% - nameStr: String containing the name of the group.
% Outputs:
% - index: Index of the group with respective name. -1 if not found.

% Find Group index of measurement type
counter=0;
for ii = 1:size(parentGroup.Groups)
    if ~contains(parentGroup.Groups(ii).Name,'acquisition')
        continue
    else
        counter=counter+1;
    end
end
