% Property of IRsweep AG 
% h5getNumberOfMeasurementsOfType
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [measType] = h5getTypeOfMeasurement(folder, filename)
% This function retrieves the type of measurement(time-resolved/static)
% stored in the hdf5
% Inputs:
% - folder: folder where to find the file
% - filename: filename of the file
% Outputs:
% - measType: type of measurements

info = h5info(strcat(folder,filename));

infogroup = h5findGroupWithName(folder,filename,'/info');

measType = get_attribute_from_group(infogroup,'Processor','unknown');

if ~contains(measType,'TimeResolved') && ~contains(measType,'TimeIntegrated')
    measType = input('Type of measurement could not be detected. Enter it (TimeResolved/TimeIntegrated) : ','s');
end

