% Property of IRsweep AG 
% readStaticProcessedData
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRsweep. All rights reserved.

function [transmission,wnAxis,evalParams] = readStaticProcessedData(dataPath, measName)
% This function loads the static data stored in a hdf5 file.
% Input:
% - dataPath: Folder where measurements are saved
% - measName: body of the filename (up to underscore before channel
% number.
% Output:
% - transmission: matrix containing transient transmissions.
% - wnAxis: vector with wavenumber axis.
% - evalParams: structure containing the evaluation parameters.

%% Load evaluation parameters
evalParams = makeEvaluationParamStructFromProcessedH5(dataPath, measName);

%% Load time and wavenumber axis
fprintf('Loading wavenumber axis : ')
wnAxis = h5read(strcat(dataPath,measName),strcat(evalParams.infogroup.Name,'/first_wn_axis'));
fprintf('Done\n')

%% Load peakMeanAmp
fprintf('Loading average peak amplitudes : ')
peakMeanAmp = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Groups(1).Name,'/peakmeanamp'));
fprintf('Done\n')

%% Load standard deviation of peaks
fprintf('Loading of the deviation of peaks : ')
stdPeak = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Name,'/info/peakstd'));
fprintf('Done\n')


%% Load transmission data
fprintf('Loading transmission : ')
for ii=1:length(evalParams.dataGroup.Groups)-1
    tmp_amp = h5read(strcat(dataPath,measName),strcat(evalParams.dataGroup.Groups(ii).Name,'/y'));
    transmission(:,ii)=tmp_amp;
end
fprintf('Done\n')

%% Save to .mat file
fprintf('Saving in mat file : ')
evalParams.processedName = strcat(erase(evalParams.processedName, '.h5'), '.mat');
evalParams.dataVariables = [{'transmission'},{'wnAxis'},{'peakMeanAmp'},{'evalParams'},{'stdPeak'}];

save(evalParams.processedName, evalParams.dataVariables{1});
for ii = 1:size(evalParams.dataVariables,2)
    save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
end
fprintf('Done\n\n')
