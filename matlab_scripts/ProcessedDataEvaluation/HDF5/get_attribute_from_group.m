% Property of IRsweep AG 
% get_attribute_from_group
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function value = get_attribute_from_group(group,nameStr,defaultVal)
% This function looks for a group name in an hdf5 file and returns the
% index of the group.
% Inputs:
% - group : the group in which we search
% - nameStr: String containing the name of the item you look
% Outputs:
% - value : the value of the attribute

% Find Group index of measurement type

g = group.Attributes; 

index = -1;
for ii = 1:length(g)
    if strcmp(g(ii).Name , nameStr)
        index=ii;
    end
end

if index >0 && isfield(g(index),'Value')
    value = g(index).Value;
else
    if exist('defaultVal','var')==1
        defaultstr = strcat(['(',num2str(defaultVal),')']);
    else
        defaultstr = '';
    end
    value = input(strcat(['Enter the value for ',nameStr,' ',defaultstr,' :  ']),'s');
    [num, status] = str2num(value);
    if status
        value = num;
    end
end
end