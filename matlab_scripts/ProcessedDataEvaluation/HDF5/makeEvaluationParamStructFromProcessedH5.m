% Property of IRsweep AG 
% makeEvaluationParamStructFromProcessedH5
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRsweep. All rights reserved.

function evalParams = makeEvaluationParamStructFromProcessedH5(dataPath, measName)
% This function makes a structure containing all important parameters for
% evaluation of a time-resolved data set
% Inputs:
% - dataPath: Path where parameters will be saved
% - fileName: Name stem of parameter file
% Outputs:
% - evalParams: structure containing the evaluation parameters

% check, if there are evaluation parameters saved for this data
% if exist(strcat(dataPath,extractBefore(measName,'raw'),'_evalParams.mat'), 'file') == 2
% evalStruct = load(strcat(dataPath,extractBefore(measName,'raw'),'_evalParams.mat'), 'evalParams');
% evalParams = evalStruct.evalParams;
% Otherwise create default evaluation parameters
% else

info = h5info(strcat(dataPath,measName));
evalParams = struct;


evalParams.infogroup = h5findGroup(info,'/info');

%% Parameters to be populated in each measurement type
%File parameters
evalParams.processedName=strcat(dataPath,measName);
evalParams.dataPath=dataPath;
evalParams.measNameStem=erase(measName,'_processed_data.h5');
evalParams.outPath = dataPath; 
evalParams.measName = measName; 
evalParams.tfName = 'transferFunction';
evalParams.dataGroup = h5findGroup(info,'/transmission');         %%TODO : needs to be changed !
evalParams.measType = get_attribute_from_group(evalParams.infogroup,'Processor');
evalParams.measType = evalParams.measType{1};
evalParams.versionServer = get_attribute_from_group(evalParams.infogroup,'SoftwareVersion','3.2.3');
evalParams.versionServer = evalParams.versionServer{1};

%Laser parameters
evalParams.centralWn = single(get_attribute_from_group(evalParams.infogroup,'CentralWaveNumber','1200/1650'));

%Acquisition parameters
evalParams.preTriggerSamples = round(single(get_attribute_from_group(evalParams.infogroup,'PreTrigSamples')));
evalParams.acquisitionFrequency = single(get_attribute_from_group(evalParams.infogroup,'AcquisitionFrequency',10));
evalParams.numberAveragesBackground = round(single(get_attribute_from_group(evalParams.infogroup,'NumberOfAveragesBackground',10)));
evalParams.numberAveragesSample = round(single(get_attribute_from_group(evalParams.infogroup,'NumberOfAveragesSample',10)));
evalParams.numberAveragesTransfer = round(single(get_attribute_from_group(evalParams.infogroup,'NumberOfAveragesTransfer',10)));
evalParams.sampRate = double(get_attribute_from_group(evalParams.infogroup,'SampleRate',2e9));
evalParams.traceLength = round(log2(single(get_attribute_from_group(evalParams.infogroup,'Samples',33554432))));
evalParams.loCut=  double(get_attribute_from_group(evalParams.infogroup,'loCut',5e7));
evalParams.hiCut= double(get_attribute_from_group(evalParams.infogroup,'hiCut',9.5e8));
evalParams.startFileNum = 1; 
evalParams.stopFileNum = size(evalParams.dataGroup.Groups,1); 

if evalParams.stopFileNum==0
    error(strcat(['No measurement found in ',evalParams.dataGroup.Name]))
end


%Processing parameters
evalParams.fftLength = round(log2(single(get_attribute_from_group(evalParams.infogroup,'FftLength',32768))));
evalParams.zeroLength = floor(single(get_attribute_from_group(evalParams.infogroup,'ZeroPadding',1)));
evalParams.interleaveFactor = floor(2^single(get_attribute_from_group(evalParams.infogroup,'Interleaving',2)));
evalParams.interleaveTimeStep = 2^evalParams.fftLength/evalParams.sampRate/evalParams.interleaveFactor;
evalParams.moduleID = get_attribute_from_group(evalParams.infogroup,'ModuleID');
evalParams.moduleID = evalParams.moduleID{1};
if ~contains([evalParams.infogroup.Attributes.Name],'MaxPeakC')
    evalParams.maxPeakNo = get_attribute_from_group(h5findGroup(evalParams.dataGroup,'/info'),'MaxPeakC')+1;
else
    evalParams.maxPeakNo = get_attribute_from_group(evalParams.infogroup,'MaxPeakC')+1; 
end
evalParams.specAvgHalfWidth = 1;

fprintf('\n\n')
end