% Property of IRsweep AG 
% h5findGroupIndexWithName
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [index] = h5findGroupIndexWithName(folder, filename, nameStr)
% This function looks for a group name in an hdf5 file and returns the
% index of the group.
% Inputs:
% - folder: folder where to find the file
% - filename: filename of the file
% - nameStr: String containing the name of the group.
% Outputs:
% - index: Index of the group with respective name. -1 if not found.

info = h5info(strcat(folder,filename));

% Find Group index of measurement type
index = -1;
for ii = 1:size(info.Groups)
    if isempty(strfind(nameStr,info.Groups(ii).Name))
        continue
    else
        index = ii;
    end
end
