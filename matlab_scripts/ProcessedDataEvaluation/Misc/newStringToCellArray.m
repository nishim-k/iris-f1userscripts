% Property of IRsweep AG 
% newStringToCellArray
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function appendedCellArray = newStringToCellArray (cellArray, str)
% This function appends a string to a cell array, if the given string is
% not already present in the cell array.
% Inputs:
% - cellArray: cell array
% - str: string to be appended
% Outputs:
% - appendedCellArray: cell array with new element

% Find if / where the cell array contains the string
strPos = findStringInCellArray (cellArray, str);

% append the string at the end of the cell array
if strPos < 0
    appendedCellArray = [cellArray {str}];
else
    appendedCellArray = cellArray;
end