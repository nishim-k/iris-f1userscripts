% Property of IRsweep AG 
% stdVsPeakNumber
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [stdPeak, evalParams] = stdVsPeakNumber(evalParams, plotOn)
% This function calculates the standard deviation of each peak in a
% transient transmission file.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - plotOn: whether or not to plot the standard deviation
% Outputs:
% - avgTransienTrans: the averaged transient transmission
% - timeAxis: corresponding time axis
% - stdAxis: array containing std vs peak number
% - evalParams: used evaluation parameters

for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

if ~exist('stdPeak','var')
    saveOn = true;
else
    saveOn = false;
end

if  contains(evalParams.measType,'TimeResolved')
    
    preTriggerSlices = floor(evalParams.preTriggerSamples/evalParams.sampRate/evalParams.interleaveTimeStep);
    if preTriggerSlices < 2
        error('need more pre-trigger samples to calculate a standard deviation')
    else
        %Assignment of the pretrigger data variable
        preTrig = avgTransientTrans(1:preTriggerSlices,:);

        %Calculation of variances of all peaks at different time instances
        stdPeak = sqrt(var(preTrig, 1));
    end

    if size(stdPeak,2)>1
        stdPeak=transpose(stdPeak);
    end
    
    if saveOn
        fprintf('Saving in mat file : ')
        % Save data to .mat file
        evalParams.processedName = strcat(evalParams.dataPath, evalParams.measNameStem,  '_avgOfFiles', num2str(evalParams.startFileNum),'-',num2str(evalParams.stopFileNum),'.mat');
        evalParams.dataVariables = newStringToCellArray (evalParams.dataVariables, 'stdPeak');
        save(evalParams.processedName, evalParams.dataVariables{1});
        for ii = 1:size(evalParams.dataVariables,2)
            save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
        end
    end
    
elseif ~exist('stdPeak','var') && contains(evalParams.measType,'TimeIntegrated')
    error('no stdPeak computation available for time integrated measurement')
else
    fprintf('stdPeak already loaded from hdf5\n')
end


if plotOn
    figure
    semilogy(stdPeak);
    xlabel('peak number')
    ylabel('rms deviation of transmission')
    title('plot from stdVsPeakNumber-function')
end

end






