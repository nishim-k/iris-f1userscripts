% Property of IRsweep AG 
% findStringInCellArray
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function strPos = findStringInCellArray (cellArray, str)
% This function finds a string in a cell array and returns its position.
% Inputs:
% - cellArray: cell array
% - str: string to find
% Outputs:
% - strPos: index of the string in the cell array, -1 if not present.

strPos = -1;
for ii = 1:size(cellArray,2)
    if isempty( strfind(cellArray{ii}, str) )
        
    else
        strPos = ii;
    end
end