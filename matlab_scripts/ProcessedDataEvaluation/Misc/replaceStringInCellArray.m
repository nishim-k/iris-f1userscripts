%Property of IRsweep AG 
% replaceStringInCellArray
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function newCellArray = replaceStringInCellArray (cellArray, oldStr, newStr)
% This function appends a string to a cell array, if the given string is
% not already present in the cell array.
% Inputs:
% - cellArray: cell array
% - str: string to be appended
% Outputs:
% - appendedCellArray: cell array with new element

% Find if / where the cell array contains the string
strPos = findStringInCellArray (cellArray, oldStr);

% replace the string in the cell array
if strPos < 0
    error('did not find string to replace')
else
    newCellArray = cellArray;
    newCellArray{strPos} = newStr;
end