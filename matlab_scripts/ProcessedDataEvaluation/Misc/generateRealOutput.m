% Property of IRsweep AG 
% generateRealOutput
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [realTransients, timeAxis, evalParams] = generateRealOutput(evalParams, format, plotOn)
% This function transforms complex transmission into an output format
% specified by the user.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - format: string containing the requested output format. Possible values
% are 'transmission, 'absorption', or 'absorbance'.
% - plotOn: whether or not to plot the transformed data.
% Outputs: 
% - realTransients: 2D matrix of the data transformed to the requested
% format.
% - timeAxis: corresponding time axis
% - evalParams: used evaluation parameters

% load data
for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

% calculate requested output format
switch format
    case 'transmission'
        realTransients = real(avgTransientTrans);
    case 'absorption'
        realTransients = 1 - real(avgTransientTrans);
    case 'absorbance'
        realTransients = -log10(real(avgTransientTrans));
    otherwise
        error('unknown format')
end

% save data to file
save(evalParams.processedName, evalParams.dataVariables{1});
for ii = 1:size(evalParams.dataVariables,2)
    save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
end

% plot one transient if requested
if plotOn
    figure
    plot(timeAxis, realTransients(:,evalParams.maxPeakNo))
    xlabel('time (s)');
    ylabel(format);
    title('plot from generateRealOutput-function')
end




