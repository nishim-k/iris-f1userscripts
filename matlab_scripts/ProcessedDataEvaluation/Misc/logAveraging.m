function [logX, logY] = logAveraging(dataX, dataY, noPoints, plotOn)
% This function puts data on a logarithmic x-scale and averages data points
% logarithmically.
% Inputs:
% - dataX: x-axis of data
% - dataY: y-data of data
% - noPoints: number of points wanted in the logarithmic representation
% - plotOn: whether or not to plot outcome
% Outputs:
% - logX: logarithmically scales x-axis of data
% - logY: logarithmically averaged y-data of data

% remove all values at x < 0
data(:,1) = dataX(dataX>0);
data(:,2) = dataY(dataX>0);

% sort your data to increasing x axis
data = sortrows(data);

% make log-spaced x-axis
logX = logspace(log10(data(1,1)), log10(data(end,1)), noPoints);
logY = zeros(1,noPoints);

% loop through logX and average the y-data
startInd = 1;
for ii = 1: noPoints
    [~, stopInd] = min(abs(data(:,1)-logX(ii)));
    logY(ii) = mean(data(startInd:stopInd,2));
    logX(ii) = (data(startInd,1) + data(stopInd,1)) /2;
    startInd = stopInd + 1;
end

% remove non-finite values from logY and logX
keepVal = isfinite(logY);
logX = logX(keepVal);
logY = logY(keepVal);

% plot result
if plotOn
    figure
    semilogx(logX, logY)
end

