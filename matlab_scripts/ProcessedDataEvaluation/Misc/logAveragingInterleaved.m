function [logX, logY] = logAveragingInterleaved(dataX, dataY, noPoints, interleaveFact, plotOn)
% This function puts data on a logarithmic x-scale and averages data points
% logarithmically. The interleave factor increases the data range used for
% averaging, such that neighboring points use overlapping data ranges.
% Inputs:
% - dataX: x-axis of data
% - dataY: y-data of data
% - noPoints: number of points wanted in the logarithmic representation
% - interleaveFact: the larger, the more overlap have neighboring points
% (e.g. 2 --> each data point is used in two log-datapoints)
% - plotOn: whether or not to plot outcome
% Outputs:
% - logX: logarithmically scales x-axis of data
% - logY: logarithmically averaged y-data of data

% remove all values at x < 0
data(:,1) = dataX(dataX>0);
data(:,2) = dataY(dataX>0);

% sort your data to increasing x axis
data = sortrows(data);

% make log-spaced x-axis
logX = logspace(log10(data(1,1)), log10(data(end,1)), noPoints);
logY = zeros(1,noPoints);

% loop through logX and average the y-data
startInd = 1;
for ii = 1: noPoints
    [~, stopInd] = min(abs(data(:,1)-logX(ii).*interleaveFact));
    logY(ii) = mean(data(startInd:stopInd,2));
    logX(ii) = sqrt(data(startInd,1)*data(stopInd,1));
    [~, startInd] = min(abs(data(:,1)-logX(ii)));
end

% remove non-finite values from logY and logX
keepVal = isfinite(logY);
logX = logX(keepVal);
logY = logY(keepVal);

% plot result
if plotOn
    figure
    semilogx(logX, logY)
end

