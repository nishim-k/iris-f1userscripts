% Property of IRsweep AG 
% readLaserConditionsFromH5
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [current, temperature] = readLaserConditionsFromH5(evalParams)
% Returns temperature and current of the 1st laser (Sample in GUI)
% Inputs:
%   - evalParams: evaluation parameters
% Outputs:
%   - current: the operating current of the measurement
%   - temperature: the opearting temperature of the measurement



infogroup = h5findGroup(evalParams.dataGroup.Groups(1),'/info');
attributes = infogroup.Attributes
for ii = 1 : size(attributes, 1)
    if strcmp(attributes(ii).Name, 'Laser1Current') || strcmp(attributes(ii).Name, 'SampleCurrent')
       current = attributes(ii).Value; 
    end
    if strcmp(attributes(ii).Name, 'Laser2Temperature') || strcmp(attributes(ii).Name, 'SampleTemperature')
       temperature = attributes(ii).Value; 
    end
    if strcmp(attributes(ii).Name, 'Laser1Temperature') || strcmp(attributes(ii).Name, 'LocalOscillatorTemperature')
       temperature = attributes(ii).Value; 
    end
end

if (exist('current', 'var') == 0) || (exist('temperature', 'var') == 0)
    error('Error reading temperature or current from h5 file.')
end