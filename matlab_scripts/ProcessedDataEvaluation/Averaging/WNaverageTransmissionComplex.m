% Property of IRsweep AG 
% WNaverageTransmissionComplex
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function peakAvg=WNaverageTransmissionComplex(evalParams,startPeak,stopPeak,weightsOn)
%weight and average along WN direction

% load data
for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

if weightsOn
    weigthPeak = weightingFromStd(startPeak,stopPeak,stdPeak);
    peakAvg = mean(avgTransmission(startPeak:stopPeak).*weigthPeak*(stopPeak-startPeak+1));
    
else
    peakAvg = mean(avgTransmission(startPeak:stopPeak));
end
    
    
end