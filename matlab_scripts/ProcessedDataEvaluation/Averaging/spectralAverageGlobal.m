% Property of IRsweep AG 
% spectralAverageOfStaticTransmission
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [avgSpectrum, evalParams] = spectralAverageGlobal(evalParams, specAvgHalfWidth, plotOn)
% This function performs an averaging of multiple, spectrally neighboring
% datapoints for static and time-resolved data
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - specAvgHalfWidth: averaging will take 2*specAvgHalfWidth+1 datapoint
% into account
% - plotOn: whether or not to plot the averaged data.
% Outputs:
% - avgSpectrum: the averaged transmission or transient
% - evalParams: used evaluation parameters


for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

if contains(evalParams.measType,'TimeResolved')
   [avgSpectrum,~,evalParams] = spectralAverageOfTransientTransmission(evalParams,specAvgHalfWidth,plotOn); 
elseif ~exist('stdPeak','var') && contains(evalParams.measType,'TimeIntegrated')
        error('No spectral averaging for static data without stdPeak in hdf5')
elseif exist('stdPeak','var') && contains(evalParams.measType,'TimeIntegrated') 
    
    [avgSpectrum,evalParams] = spectralAverageOfStaticTransmission(evalParams,specAvgHalfWidth,plotOn);
else
        error('Measurement type not recognize')
end 

    
end
