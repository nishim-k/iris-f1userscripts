% Property of IRsweep AG 
% spectralAverageOfTransientTransmission
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [avgTransientTrans, timeAxis, evalParams] = spectralAverageOfTransientTransmission(evalParams, specAvgHalfWidth, plotOn)
% This function performs an averaging of multiple, spectrally neighboring
% datapoints.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - specAvgHalfWidth: averaging will take 2*specAvgHalfWidth+1 datapoint
% into account
% - plotOn: whether or not to plot the averaged data.
% Outputs:
% - avgTransienTrans: the averaged transient transmission
% - timeAxis: corresponding time axis
% - evalParams: used evaluation parameters

% load data
fprintf('Spectral averaging using weights on pretrigger samples : ')
for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

% check if data was already spectrally averaged
if evalParams.specAvgHalfWidth > 1
    error('This data has already been spectrally averaged. Do not average twice!')
    
    % go through spectrum and perform a spectral average on each datapoint
else
    
    specAvgTransientTrans = avgTransientTrans;
    for ii = 1:size(avgTransientTrans,2)
        specAvgTransientTrans(:,ii) = WNaverageTransientComplex(evalParams,avgTransientTrans,max(1,ii-specAvgHalfWidth), min(size(avgTransientTrans,2), ii+specAvgHalfWidth), true);
    end
    oldAvgTransientTrans = avgTransientTrans;
    avgTransientTrans = specAvgTransientTrans;
    fprintf('Done\n')
    
    fprintf('Saving in mat file : ')
    % write data into evalParams structure and save to file
    evalParams.specAvgHalfWidth = specAvgHalfWidth;
    evalParams.processedName = strcat(evalParams.dataPath, evalParams.measNameStem,  '_processed_avgOfFiles', num2str(evalParams.startFileNum),'-',num2str(evalParams.stopFileNum),'.mat');
    save(evalParams.processedName, evalParams.dataVariables{1});
    for ii = 1:size(evalParams.dataVariables,2)
        save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
    end
    fprintf('Done\n\n')
    
    % plot data if requested
    if plotOn
        figure
        hold on
        plot(timeAxis,real(oldAvgTransientTrans(:, evalParams.maxPeakNo)))
        plot(timeAxis, real(avgTransientTrans(:, evalParams.maxPeakNo)));
        xlabel('time (s)')
        ylabel('transmission')
        title('plot from averageTransientAcquisitions-function')
        legend('Before spectral averaging','After spectral averaging')
    end
end
end