% Property of IRsweep AG 
% spectralAverageOfTransientTransmission
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [avgTransmission, evalParams] = spectralAverageOfStaticTransmission(evalParams, specAvgHalfWidth, plotOn)
% This function performs an averaging of multiple, spectrally neighboring
% datapoints.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - specAvgHalfWidth: averaging will take 2*specAvgHalfWidth+1 datapoint
% into account
% - plotOn: whether or not to plot the averaged data.
% Outputs:
% - avgTransmission: the averaged transmission
% - evalParams: used evaluation parameters

fprintf('Spectral averaging using weights from stdPeak : ')

% load data
for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

% check if data was already spectrally averaged
if evalParams.specAvgHalfWidth > 1
    error('This data has already been spectrally averaged. Do not average twice!')
    
elseif ~exist('stdPeak','var')
    error('Cannot do spectral averaging without std.')
    
    
    % go through spectrum and perform a spectral average on each datapoint
else
    specAvgTransmission = avgTransmission;
    for ii = 1:size(avgTransmission,1)
        specAvgTransmission(ii) = WNaverageTransmissionComplex(evalParams,max(1,ii-specAvgHalfWidth), min(size(avgTransmission,1), ii+specAvgHalfWidth), true);
    end
    oldavgTransmission=avgTransmission;
    avgTransmission = specAvgTransmission;    
    
    fprintf('Done\n')
    
    fprintf('Saving in mat file : ')
    % write data into evalParams structure and save to file
    evalParams.specAvgHalfWidth = specAvgHalfWidth;
    evalParams.processedName = strcat(evalParams.dataPath, evalParams.measNameStem,  '_processed_avgOfFiles', num2str(evalParams.startFileNum),'-',num2str(evalParams.stopFileNum),'.mat');
    save(evalParams.processedName, evalParams.dataVariables{1});
    for ii = 1:size(evalParams.dataVariables,2)
        save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
    end
    fprintf('Done\n\n')
    
    if plotOn
        figure()
        hold on
        plot(wnAxis,real(oldavgTransmission));
        plot(wnAxis, real(avgTransmission));
        xlabel('wavenumber (cm-1)')
        ylabel('transmission')
        title('plot from spectralAverageGlobal-function')
        legend('Before spectral averaging','After spectral averaging')
    end
end
end