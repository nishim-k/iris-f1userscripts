%Property of IRsweep AG 
% weighting_funcComplex
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function weights = weightingFromStd( startNum,stopNum,stdPeak )
%This function calculates the weights associated with the lines. This is
%important to reject the peaks with less weights and study the absorbance
%values rendered from significant peak amplitudes. It asks for a staring
%index and a ending index.
% Input : 
% - startNum: the first index to average
% - stopNum : the last index to average
% Outputs :
% - weights: A 1D array containing the relative weights associated with peaks
% of size stopNum-startNum

% Variance
    T = stdPeak(startNum:stopNum).^2;


%Inverse of variances  
    T=1./T; 
    
%Sum of variances
    SL=sum(T);

%Divide Inverse of variance by the sum to get the weights
    T=T./SL;
    weights=T;
end