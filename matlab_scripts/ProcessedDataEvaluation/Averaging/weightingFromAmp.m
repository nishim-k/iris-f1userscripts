%Property of IRsweep AG 
% weighting_funcComplex
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function weights = weightingFromAmp( peakAmp,channel )
%This function calculates the weights associated with the peaks in channel 3. This is
%important to reject the peaks with less weights and study the absorbance
%values rendered from significant peak amplitudes. It takes the dataset of
%dimensions AxBx3 and returns 1xB.
% Input : 
% - peakAmp: the dataset from channel 1,2 and 3
% Outputs :
% - weights: A 1D array containing the relative weights associated with peaks

%Assignment of the dataset from channel 3 to a variable
    row1= peakAmp(:,:,channel);
    
%Calculation of variances of all peaks at different time instances
    T= std(row1,1);

%Inverse of variances  
    T=1./T; 
    
%Sum of variances
    SL=sum(T);

%Divide Inverse of variance by the sum to get the weights
    T=T./SL;
    weights=T;
end
