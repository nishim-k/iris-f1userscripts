% Property of IRsweep AG 
% WNaverageTransientComplex
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function peakAvg=WNaverageTransientComplex(evalParams, transientSpec,startPeak,stopPeak,weightsOn)
%weight and average along WN direction

preTriggerSlices = floor(evalParams.preTriggerSamples/evalParams.sampRate/evalParams.interleaveTimeStep);


if weightsOn
    weights = weightingFromAmp(transientSpec(1:preTriggerSlices,startPeak:stopPeak),1);
    weights = weights.*(stopPeak-startPeak+1);
    weights = repmat(weights,size(transientSpec,1),1);
    peakAvg = mean(transientSpec(:,startPeak:stopPeak,:).*weights,2);
    
else
    peakAvg = mean(transientSpec(:,startPeak:stopPeak,:),2);
end
    
    
end