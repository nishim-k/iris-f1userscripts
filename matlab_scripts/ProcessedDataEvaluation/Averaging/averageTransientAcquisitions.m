% Property of IRsweep AG 
% averageTransientAcquisitions
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [avgTransientTrans, timeAxis, evalParams] = averageTransientAcquisitions(evalParams, startFileNum, stopFileNum, plotOn)
% This function averages the transient transmission of multiple files to one
% singe transient transmission and saves the outcome as a .mat file
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - startFileNum: first acquisition to use for averaging. Choose -1 for
% averaging all acquisitions.
% - stopFileNum: last acquisition to use for averaging
% - plotOn: whether or not to plot the average transient
% Outputs:
% - avgTransienTrans: the averaged transient transmission
% - timeAxis: corresponding time axis
% - evalParams: used evaluation parameters

for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end
if startFileNum > 0
    % start and stop index needed if processing did not start at the first
    % acquisition
    startIndex = startFileNum - evalParams.startFileNum + 1;
    stopIndex = stopFileNum - evalParams.startFileNum + 1;
    fprintf('Averaging %d files : ',stopIndex-startIndex+1);
    avgTransientTrans = mean(transientTrans(:,:,startIndex:stopIndex),3);
    evalParams.startFileNum = startFileNum;
    evalParams.stopFileNum = stopFileNum;
else
    fprintf('Averaging %d files : ',size(transientTrans,3));
    avgTransientTrans = mean(transientTrans(:,:,:),3);
end
fprintf('Done\n')

% Save data to .mat file
fprintf('Saving in mat file : ')
evalParams.processedName = strcat(evalParams.dataPath, evalParams.measNameStem,  '_processed_avgOfFiles', num2str(evalParams.startFileNum),'-',num2str(evalParams.stopFileNum),'.mat');
evalParams.dataVariables = replaceStringInCellArray(evalParams.dataVariables,'transientTrans','avgTransientTrans');
save(evalParams.processedName, evalParams.dataVariables{1});
for ii = 2:size(evalParams.dataVariables,2)
    save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
end
fprintf('Done\n\n\n')

if plotOn
    figure
    plot(timeAxis, real(avgTransientTrans(:, evalParams.maxPeakNo)));
    xlabel('time (s)')
    ylabel('transmission')
    title('plot from averageTransientAcquisitions-function')
end
end


