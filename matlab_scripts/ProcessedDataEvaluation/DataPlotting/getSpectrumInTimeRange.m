% Property of IRsweep AG 
% getSpectrumInTimeRange
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [transmissionSpec, evalParams] = getSpectrumInTimeRange(evalParams, startTime, stopTime, plotOn)
% This function averages multiple spectra in a given time range of a 
% transient transmission spectrum and returns the averaged spectrum.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - startTime: first time-point to be used for averaging.
% - stopTime: last time-point to be used for averaging.
% - plotOn: whether or not to plot the calculated spectrum.
% Outputs:
% - trasmissionSpec: averaged transmission spectrum
% - evalParams: used evaluation parameters

% load data
load(evalParams.processedName);

% Find indices corresponding to wavenumber
[~, startTimeIndex] = min(abs(timeAxis-startTime));
[~, stopTimeIndex] = min(abs(timeAxis-stopTime));

% Average transmission over time range
transmissionSpec = mean(avgTransientTrans(startTimeIndex:stopTimeIndex,:),1);

if plotOn
    figure
    if exist('wnAxis','var')
        plot(wnAxis, real(transmissionSpec));
        xlabel('wavenumber (cm^{-1})')
    else
        plot(real(transmissionSpec));
        xlabel('peak number')
    end
    ylabel('transmission')
    title('plot from getSpectrumInTimeRange-function')
end
