% Property of IRsweep AG 
% plotTransientInWnRange
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [fig_obj,transient, evalParams] = plotTransientInWnRange(fig_obj,evalParams, minWn, maxWn,timeRes,label)
% This function plots the absorbance for a definite range of time on the
% figure specified.
%
% Inputs:
% - fig_obj : the figure on which you want to plot
% - evalParams : the structure containing all the evaluation parameters
% - minWn : the minimal wn to average
% - maxWn : the maximal wn to average
% - timeRes : the time resolution in s
% - label : the name of the measurement if any
% Ouputs:
% - fig_obj : the figure on which it was plotted
% - transient    : the real transient absorbance
% - evalParams : the structure containing all the evaluation parameters

%% Get the spectrum in the range of time 
[transient, timeAxis,wnEval, evalParams] = getTransientWithTimeResolution(evalParams, minWn, maxWn, timeRes, false);

%% Load the wavenumber axis from the mat file
load(evalParams.processedName,'wnAxis')

%% Set label
if(exist('label', 'var') == 1)
    labellegend=strcat([label,' at ',num2str(wnEval,'%5.1f'),'cm^{-1}']);
else
    labellegend=strcat([num2str(wnEval),'cm^{-1}']);
end

transient = -log10(real(transient));

%% Plot it
figure(fig_obj);
hold on;
plot(timeAxis,transient,'DisplayName',labellegend);
legend('location','northeast')

end