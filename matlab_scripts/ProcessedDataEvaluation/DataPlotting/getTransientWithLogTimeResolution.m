% Property of IRsweep AG 
% getTransientWithLogTimeResolution
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [transient, timeAxis,wnEval, evalParams] = getTransientWithLogTimeResolution(evalParams, minWn, maxWn, noTimeSteps, plotOn)
% This function averages multiple wavelengths of a transient transmission
% spectrum and returns the averaged transient.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - minWn: lower wavenumber to be averaged
% - maxWn: upper wavenumber to be averaged
% - plotOn: whether or not to plot the calculated transient.
% Outputs:
% - transient: transient transmission averaged betwen min and max
% wavenumber
% - timeAxis: the time axis of the transient in log scale with resolution
% - wnEval: the center wavenumber at which it is evaluated
% - evalParams: used evaluation parameters

[transient, timeAxis,wnEval, evalParams] = getTransientInWnRange(evalParams, minWn, maxWn, false);

[timeAxis, transient] = logAveragingInterleaved(timeAxis, transient, noTimeSteps, 2, false);

if plotOn
    figure
    semilogx(timeAxis, real(transient));
    xlabel('time (s)')
    ylabel('transmission')
    title('plot from getTransientWithLogTimeResolution-function')
end