% Property of IRsweep AG 
% plotSpectrumInTime
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [fig_obj,spec, evalParams] = plotSpectrumInTimeRange(fig_obj,evalParams,startTime,stopTime,thresholdCommon,label)
% This function plots the absorbance for a definite range of time on the
% figure specified.
%
% Inputs:
% - fig_obj : the figure on which you want to plot
% - evalParams : the structure containing all the evaluation parameters
% - startTime : the first time slice to average in s
% - stopTime : the last time slice to average in s
% - thresholdCommon : the threshold to hide peaks with to high std
% deviation
% - label : the name of the measurement if any
% Ouputs:
% - fig_obj : the figure on which it was plotted
% - spec    : the spectrum in real absorbance units
% - evalParams : the structure containing all the evaluation parameters

%% Get the spectrum in the range of time 
[spec, evalParams] = getSpectrumWithNoiseThreshold(evalParams, startTime, stopTime, thresholdCommon, false);

%% Load the wavenumber axis from the mat file
load(evalParams.processedName,'wnAxis')

%% Set label
if(exist('label', 'var') == 1)
    labellegend=strcat([label,' ',num2str(startTime*1000),' to ',num2str(stopTime*1000),'ms']);
else
    labellegend=strcat([num2str(startTime*1000),' to ',num2str(stopTime*1000),'ms']);
end

spec = -log10(real(spec));

%% Plot it
figure(fig_obj);
hold on;
plot(wnAxis,spec,'DisplayName',labellegend);
legend('location','northeast')

end