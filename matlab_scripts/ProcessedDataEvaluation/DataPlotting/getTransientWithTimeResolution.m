  % Property of IRsweep AG 
% getTransientWithTimeResolution
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [transient, timeAxis,wnEval, evalParams] = getTransientWithTimeResolution(evalParams, minWn, maxWn, timeRes, plotOn)
% This function averages multiple wavelengths of a transient transmission
% spectrum and returns the averaged transient with a specific time
% resolution.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - minWn: lower wavenumber to be averaged
% - maxWn: upper wavenumber to be averaged
% - plotOn: whether or not to plot the calculated transient.
% Outputs:
% - transient: transient transmission averaged betwen min and max
% wavenumber
% - timeAxis: the time axis of the transient with specific time resolution
% - wnEval: the center wavenumber at which the transient is evaluated
% - evalParams: used evaluation parameters

[transient, timeAxis,wnEval, evalParams] = getTransientInWnRange(evalParams, minWn, maxWn, false);

smoothIndex = round(timeRes./evalParams.interleaveTimeStep);
smoothIndex = max(smoothIndex,1); % make sure its larger than 1
smoothIndex = min(smoothIndex, floor(size(transient,1)/2)); % make sure its smaller than half of dataset length

transient = smooth(transient,smoothIndex);

if plotOn
    figure
    plot(timeAxis, real(transient));
    xlabel('time (s)')
    ylabel('transmission')
    title('plot from getTransientWithTimeResolution-function')
end
