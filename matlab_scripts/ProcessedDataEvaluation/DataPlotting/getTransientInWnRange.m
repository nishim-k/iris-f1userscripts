% Property of IRsweep AG 
% getTransientInWnRange
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [transient, timeAxis,wnEval, evalParams] = getTransientInWnRange(evalParams, minWn, maxWn, plotOn)
% This function averages multiple wavelengths of a transient transmission
% spectrum and returns the averaged transient.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - minWn: lower wavenumber to be averaged
% - maxWn: upper wavenumber to be averaged
% - plotOn: whether or not to plot the calculated transient.
% Outputs:
% - transient: transient transmission averaged betwen min and max
% wavenumber
% - timeAxis: the time axis of the transient in s
% - wnEval : the average wavenumber around which the transient is averaged
% - evalParams: used evaluation parameters

% load data
load(evalParams.processedName, 'avgTransientTrans', 'timeAxis', 'wnAxis', 'evalParams');

% Find indices corresponding to wavenumber
[~, minWnIndex] = min(abs(wnAxis-minWn));
[~, maxWnIndex] = min(abs(wnAxis-maxWn));

wnEval = (wnAxis(minWnIndex)+wnAxis(maxWnIndex))/2;

% Average transmission over wavenumber range
transient = WNaverageTransientComplex(evalParams, avgTransientTrans, min(minWnIndex,maxWnIndex), max(minWnIndex,maxWnIndex), true);

if plotOn
    figure
    plot(timeAxis, real(transient));
    xlabel('time (s)')
    ylabel('transmission')
    title('plot from getTransientInWnRange-function')
end
