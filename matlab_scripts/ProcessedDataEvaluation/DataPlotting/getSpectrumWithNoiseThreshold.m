% Property of IRsweep AG 
% getSpectrumWithNoiseThreshold
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


function [transmissionSpec, evalParams] = getSpectrumWithNoiseThreshold(evalParams, startTime, stopTime, threshold, plotOn)
% This function averages multiple spectra in a given time range of a 
% transient transmission spectrum, removes values lying below a specified 
% threshold, and returns the averaged spectrum.
% Inputs:
% - evalParams: structure containing full path and name of processed .mat file
% - startTime: first time-point to be used for averaging.
% - stopTime: last time-point to be used for averaging.
% - threshold: standard deviation threhold, above which values are deleted.
% - plotOn: whether or not to plot the calculated spectrum.
% Outputs:
% - trasmissionSpec: averaged transmission spectrum
% - evalParams: used evaluation parameters

% load data
load(evalParams.processedName);

% calculate std axis
[ stdPeak, ~] = stdVsPeakNumber(evalParams, false);

% Find indices corresponding to wavenumber
[~, startTimeIndex] = min(abs(timeAxis-startTime));
[~, stopTimeIndex] = min(abs(timeAxis-stopTime));

% Average transmission over time range
transmissionSpec = mean(avgTransientTrans(startTimeIndex:stopTimeIndex,:),1);

% Remove values where std is too large
aboveThreshold = stdPeak > threshold;
transmissionSpec(aboveThreshold) = NaN;

if plotOn
    figure
    if exist('wnAxis','var')
        plot(wnAxis, real(transmissionSpec));
        xlabel('wavenumber (cm^{-1})')
    else
        plot(real(transmissionSpec));
        xlabel('peak number')
    end
    ylabel('transmission')
    title('plot from getSpectrumWithNoiseThreshold-function')
end
