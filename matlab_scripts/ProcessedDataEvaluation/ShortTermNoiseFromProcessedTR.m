% Property of IRsweep AG 
% ShortTermNoiseFromProcessedTR
% Created by Quentin Saudan on 30/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.


%% Clearing of the old variables
clear all;
close all;
%% New data structure, new evaluation
dataPath = 'XXXXXXXXX\XXXXXXXXX\';
measName = 'XXXXXXXX_processed_data.h5';

saveFigsOn = true;

%% Load data from the processed hdf5 file
[transientTrans,timeAxis,~,evalParams] = readTimeResolvedProcessedData(dataPath, measName);

%% Analyse data 

fileNum = 1;

calcAllanInSec(real(transientTrans(:,evalParams.maxPeakNo,fileNum)),1/evalParams.interleaveTimeStep,40, strcat('ratio',{' of peak '},{num2str(evalParams.maxPeakNo)}),true);

FourierSpectrum(detrend(real(transientTrans(:,evalParams.maxPeakNo,fileNum))), evalParams.interleaveTimeStep, true, strcat('ratio', ' of peak', num2str(evalParams.maxPeakNo)));

if saveFigsOn
    h =  findobj('type','figure');
    savefig(h,strcat(dataPath,evalParams.measNameStem,'.fig'))
end
    

