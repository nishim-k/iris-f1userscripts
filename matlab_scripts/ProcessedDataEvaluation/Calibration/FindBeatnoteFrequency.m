% Property of IRsweep AG 
% FindBeatnoteFrequency
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [Frep] = FindBeatnoteFrequency(laserNumber, temperature, current)
% This function returns spacing [Hz] of a laser at given
% temperature and current based on laser characterization data.
% Inputs:
%   - laserName: laser identification name
%   - temperature: temperature of interest
%   - current: current of interest
% Outputs:
%   - spacing: the beatnote value at given temp. and current in GHz

% The beating function is calculated based on fit of laser characterization
% data. The beating is dependent on current and temperature. Quadratic fit
% is used for current and linear for temperature.
% Frep = a*I^2 + b*I + c + d*T

switch laserNumber
    case 1
        a = -4.00299E-02;
        b = 3.04302E-02;
        c = 9.86911E+00;
        d = -1.0E-03;
    case 2
        a = -4.01741E-02;
        b = 5.01543E-02;
        c = 9.85636E+00;
        d = -1.00009E-03;
    otherwise
        error('Given laser name was not found.');
end

Frep = (a*current^2 + b*current + c + d*temperature)*10^9;
Frep = 14.98e9;

 