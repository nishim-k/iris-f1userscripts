% Property of IRsweep AG 
% SpectrumCorrelation
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [weight, lag] = SpectrumCorrelation(calibration, unknown, correlationWeights)
% Correlates an unknown spectrum to a calibrated (broader) spectrum. Works
% similarly to cross-correlation, but the weigth is calculated based on RMS
% of the value difference in each point.
% Inputs:
%   - calibration: transmission data of a broader spectrum

%   - unknown: transmission data of a unknown spectrum
% Outputs:
%   - weigth: an array of weigth coefficients. Largest coefficient
%   represents the best fit
%   - lag: array of the displacements in terms of indicies of each weight

if (size(calibration,2) > 1) || (size(unknown,2) > 1) || size(correlationWeights,2)>1
    error('must be one row!')
end

%% Initialize arrays
sizeResult = size(calibration,1) - size(unknown,1);
weight = zeros(sizeResult,1);
lag = zeros(sizeResult,1);

%% Calculate the weight for each lag/displacement
for currentLag = 1:sizeResult
    rangeMin = currentLag;
    rangeMax = currentLag+size(unknown,1)-1;
    calibCut = calibration(rangeMin:rangeMax,1);
    
%     calibDiff = diff(calibCut);
%     measuredDiff = diff(unknown);
%     weightSlope = abs(calibDiff-measuredDiff)./(abs(calibDiff)+abs(measuredDiff));
%     weightSlope(size(unknown,1)) = 1;
%     weightAbsolute = abs(calibCut-unknown)./(abs(calibCut)+abs(unknown));
%     currentWeights = correlationWeights.*weightAbsolute.*weightSlope;
    
    calibDiff = diff(calibCut);
    measuredDiff = diff(unknown);
    weightSlope = abs(calibDiff-measuredDiff);
    weightSlope(size(unknown,1)) = max(weightSlope);
    weightSlope = weightSlope/max(weightSlope);
    weightAbsolute = abs(calibCut-unknown);
    weightAbsolute = weightAbsolute/max(weightAbsolute);
    currentWeights = correlationWeights.*(1*weightAbsolute+weightSlope);
    
    weight(currentLag, 1) = -sum(currentWeights);
    lag(currentLag, 1) = currentLag-1;
end