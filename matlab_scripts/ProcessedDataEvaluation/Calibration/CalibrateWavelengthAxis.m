% Property of IRsweep AG 
% CalibrateWavelengthAxis
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [firstPeakWn, reversed,evalParams] = CalibrateWavelengthAxis(transmission, wnSpacing, peakWeights, evalParams, minWn, maxWn)
% This function returns wavelength number of the first peak calculated by cross-correlating the
% transmission spectrum with the calibration sample spectrum.
% Inputs:
%   - transmission : the measurement done by IRisF1 in static mode
%   - wnSpacing : the wavenumber spacing in cm-1
%   - peakWeigths : the weight of each peak
%   - evalParams : structure containing all the evluation parameter
%   - minWn : the minimal wavenumber where to look for a match(Optional)
%   - maxWn : the maximal wavenumber where to look for a match(Optional)
% Outputs:
%   - firstPeakWn: the wavelength of the first peak
%   - reversed: states whether the measurement has correct orientation (0 -
%     correct, 1 - reversed)

%% Initialization of general variables
load(evalParams.calibReferencePath);

if (exist('calibTr', 'var') == 0) || (exist('calibWn', 'var') == 0)
    error('Error while loading data from calibReferencePath')
end

plotOn = 0;

%% Search for maxWn and minWn
if nargin <=4
    if isfield(evalParams,'centralWn')  
        minWn = evalParams.centralWn-size(transmission,1)*wnSpacing;
        maxWn = evalParams.centralWn+size(transmission,1)*wnSpacing;
    else
        error('no centralWn found. Add minWn and maxWn to the list of parameters')
    end
end
    

%% If multiple transmission specturm -> average them together
if size(transmission,2)>1
    transmission=mean(transmission,2);
end
if size(peakWeights,2)>1
    peakWeights=transpose(peakWeights);
end

%% Fine spectrum correlation (up to 0.01 cm-1) 
correlationMax = -10E10;
transMeasured = transmission;
transReversed = transpose(fliplr(transpose(transMeasured))); 
peakWeights = peakWeights/max(peakWeights);
correlationWeights = abs(peakWeights-max(peakWeights));
correlationWeightsReversed = fliplr(correlationWeights);
tuneWnStep = 0.01;

for jj = 0:tuneWnStep:wnSpacing
    clear calibTrResized calibWnResized
    
    % Rescale calibration FTIR data to have same spacing as calibraiton
    % measurement
    calibWnResized = (minWn+jj):wnSpacing:(maxWn+jj);
    calibTrResized = transpose(interp1(calibWn, calibTr, calibWnResized));

    % Cross-correlate the calibrated FTIR spectrum with the measrued unknown
    % spectrum. Do this both for normal orientation and reversed (flipped)
    % transmission. Due to our measurement, we dont know for sure how it is
    % oriented.
    [corrNor, ~] = SpectrumCorrelation(calibTrResized, transMeasured, correlationWeights);
    [maxNor, maxLagNor] = max(corrNor); % Attention!! -1e10 to be removed!!
    [corrRev, ~] = SpectrumCorrelation(calibTrResized, transReversed, correlationWeightsReversed);
    [maxRev, maxLagRev] = max(corrRev);

    if maxNor > correlationMax
        correlationMax = maxNor;
        correlationShift = maxLagNor;
        isReversed = 0;
        fineTuneWn = jj;  
    end
    if maxRev > correlationMax
        correlationMax = maxRev;
        correlationShift = maxLagRev;
        isReversed = 1;
        fineTuneWn = jj;
    end
end

%% Set output variables
firstPeakWn = minWn + fineTuneWn + correlationShift*wnSpacing;
reversed = isReversed;

%% Update the evalParams structure 
evalParams.isReversed=reversed;
evalParams.firstCalibPeakWn=firstPeakWn;

%% 

if plotOn
    figure
    plot(correlationWeights);
    
    figure
    subplot(2,1,1)
    plot(corrNor)
    title('Nor')
    subplot(2,1,2)
    plot(corrRev)
    title('Rev')
    
    wnAxis = 0:1:(size(transmission)-1);
    wnAxis = wnAxis.*wnSpacing;
    wnAxis = wnAxis + firstPeakWn;
    if isReversed
        wnAxis = fliplr(wnAxis);
    end
    wnAxis = transpose(wnAxis);
    figure
    plot(calibWn, calibTr)
    hold on
    plot(wnAxis, transMeasured, 'color', 'r')
end
    
    




