% Property of IRsweep AG 
% makeWnAxisGlobal
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [wnAxis, evalParams] = makeWnAxisGlobal(evalParams,evalParamsCalib, plotOn)
% This function makes a wavenumber axis from a given wavenumber spacing and
% the wavenumber of the highest peak.
% Inputs: 
% - evalParams: structure containing full path and name of processed .mat file
% - transmission: processed transmision data from calibration measurement
%   (as a function of a peak number)
% - plotOn: whether or not to plot a spectrum as function of wavenumber.
% Outputs: 
% - avgTransienTrans: the averaged transient transmission
% - timeAxis: corresponding time axis
% - wnAxis: correspodning wavenumber axis
% - evalParams: used evaluation parameters

fprintf('Loading data : ')

if any(strcmp(evalParamsCalib.dataVariables,'avgTransmission'))
    load(evalParamsCalib.processedName,'avgTransmission');
    transmission = avgTransmission;
    clear avgTransmission;
else
    load(evalParamsCalib.processedName,'transmission');
end
load(evalParamsCalib.processedName,'stdPeak','peakMeanAmp','wnAxis');
peakMeanAmpCalib = peakMeanAmp;


if (exist('stdPeak', 'var') == 0)
    weigthsMean = ones(length(transmission),1);
else
    weigthsMean = 1./stdPeak.^2;
end


if contains(evalParams.measType,'TimeResolved')
    load(evalParams.processedName,'avgTransientTrans','peakMeanAmp');
elseif contains(evalParams.measType,'TimeIntegrated')
    load(evalParams.processedName,'avgTransmission','peakMeanAmp');
else
    error('Measurement type not recognize')
end


%% calculate spacing in wavelength numbers cm-1
wnSpacing = abs(mean(diff(wnAxis)));

minWn = evalParams.centralWn-size(transmission,1)*3/4*wnSpacing;
maxWn = evalParams.centralWn+size(transmission,1)*3/4*wnSpacing;

fprintf('Done\n')

[evalParams.firstCalibPeakWn,indx] = min(wnAxis);
if indx > 1
    evalParams.isReversed=true;
else
    evalParams.isReversed=false;
end

%% Calibrate the wavelength axis of the calibration sample 
% fprintf('Performing automatic calibration : ')
% [~, ~,evalParams] = CalibrateWavelengthAxis(transmission, wnSpacing, weigthsMean, evalParams, minWn, maxWn);
% fprintf('Done\n')
fprintf('Performing manual calibration : ')
[~, ~,evalParams] = ManualCalibration(transmission ,wnSpacing, weigthsMean, evalParams, minWn, maxWn);
fprintf('Done\n')

fprintf('Correction of the wavenumber axis : ')
% calculate how many lines behind/infront was sample measurement with
% respect to calibration measurement
[coef, lag] = xcorr(peakMeanAmpCalib, peakMeanAmp);
[~, maxIndex] = max(coef);
shift = lag(maxIndex);

% generate wavenumber axis
if contains(evalParams.measType,'TimeResolved')
    wnAxis = 0:1:(size(avgTransientTrans,2)-1);
elseif contains(evalParams.measType,'TimeIntegrated')
    wnAxis = 0:1:(size(avgTransmission,1)-1);
else
    error('Measurement type not recognize')
end
wnAxis = wnAxis.*wnSpacing;
wnAxis = wnAxis + evalParams.firstCalibPeakWn + shift*wnSpacing;

if evalParams.isReversed == 1
    wnAxis = fliplr(wnAxis);
end

% generate wavenumber axis
wnAxis_calib = 0:1:(length(transmission)-1);
wnAxis_calib = wnAxis_calib.*wnSpacing;
wnAxis_calib = wnAxis_calib + evalParams.firstCalibPeakWn;

if evalParams.isReversed == 1
    wnAxis_calib = fliplr(wnAxis_calib);
end

if size(wnAxis,2)>1
    wnAxis=transpose(wnAxis);
end

% write parameter to structure and file
evalParams.wnSpacing = wnSpacing;
evalParams.firstPeakWn = wnAxis(1);

fprintf('Done\n')

fprintf('Updating wnAxis in the mat file : ')
save(evalParams.processedName, 'wnAxis', '-append');
fprintf('Done\n')

wnAxis_measurement = wnAxis;
wnAxis = wnAxis_calib;
fprintf('Updating wnAxis in the calibration mat file : ')
save(evalParamsCalib.processedName, 'wnAxis', '-append');
fprintf('Done\n')
wnAxis = wnAxis_measurement;


if plotOn
    calibData = load(evalParams.calibReferencePath);

    if (exist('calibData', 'var') == 0)
        error('Error while loading data from calibReferencePath')
    end

    figure
    hold on
    plot(wnAxis_calib,transmission,'r')
    plot(calibData.calibWn,calibData.calibTr,'b')
    xlabel('wavenumber (cm^{-1})')
    ylabel('transmission')
    title('plot from makeWnAxis-function. The calibration measurement')
    figure
    if contains(evalParams.measType,'TimeResolved')
        plot(wnAxis, real(mean(avgTransientTrans(:,:),1)));
    elseif contains(evalParams.measType,'TimeIntegrated')
        plot(wnAxis,real(avgTransmission));
    else
        error('Measurement type not recognize')
    end
    xlabel('wavenumber (cm^{-1})')
    ylabel('transmission')
    title('plot from makeWnAxis-function')
end



