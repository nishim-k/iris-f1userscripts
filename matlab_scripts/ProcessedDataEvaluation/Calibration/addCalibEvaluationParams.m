% Property of IRsweep AG 
% addCalibEvaluationParams
% Created by Quentin Saudan on 24/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function evalParams = addCalibEvaluationParams(evalParams, calibMeasFolder, calibMeasName, calibReferencePath)
% Adds calibration path to the evalParamas
% Inputs:
%   - evalParams: evaluation parameters
%   - calibMeasFolder: folder containing calibration measurement
%   - calibMeasName: name of the calibration measurement
%   - calibFtirPath: full path to the FTIR data of the calibriation sample
% Outputs:
%   - evalParams: updated evaluation paremeters

%Load .mat file
for ii = 1:size(evalParams.dataVariables,2)
    load(evalParams.processedName, evalParams.dataVariables{ii});
end

% add to evalParams
evalParams.calibMeasFolder = calibMeasFolder;
evalParams.calibMeasName = calibMeasName;
evalParams.calibReferencePath = calibReferencePath;
if contains(calibMeasName,'raw')
    calibMeasNameCut = extractBefore(calibMeasName,'raw');
else
    calibMeasNameCut = extractBefore(calibMeasName,'processed');
end
evalParams.processedNameCalib = strcat(evalParams.calibMeasFolder, calibMeasNameCut,  'processed_data.mat');

% save to .mat file
save(evalParams.processedName, evalParams.dataVariables{1});
for ii = 1:size(evalParams.dataVariables,2)
    save(evalParams.processedName, evalParams.dataVariables{ii}, '-append');
end

