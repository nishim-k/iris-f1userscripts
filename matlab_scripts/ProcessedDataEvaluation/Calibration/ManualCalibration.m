% Property of IRsweep AG 
% ManualCalibration
% Created by Quentin Saudan on 25/10/2018.
% Copyright (c) 2018 IRSweep. All rights reserved.

function [firstPeakWn, reversed,evalParams] = ManualCalibration(transmission,wnSpacing, peakWeights, evalParams, minWn, maxWn)
% This function takes a previous wavenumber axis, plot the transmission
% according to it on the FTIR measurement and allows the user to tune and
% reverse the axis for a manual calibration. 
% Inputs: 
% - transmission: processed transmision data from calibration measurement
%   (as a function of a peak number)
% - firstPeakWn: the wavenumber of the first peak
% - reversed: boolean telling if reversed or not already
% - wnSpacing: the spacing of the wavnumber 
% - evalParams: structure containing full path and name of processed .mat file
% Outputs: 
% - wnAxis: a new wavenumber axis manually tuned

%% Check if a first calibration was done before and use it. Otherwise, built it from parameters.
if isfield(evalParams,'isReversed') && isfield(evalParams,'firstCalibPeakWn')
    firstPeakWn = evalParams.firstCalibPeakWn;
    reversed = evalParams.isReversed;
elseif isfield(evalParams,'centralWn')
    firstPeakWn = evalParams.centralWn-(size(transmission,1)*wnSpacing)/2;
    reversed = 0;
elseif nargin >4 && nargin <=6
    firstPeakWn = (maxWn+minWn)/2-length(transmission)/2*wnSpacing;
    reversed = 0;
else
    error('Could not find previous calibration. Add maxWn and minWn to the parameters')
end
    

if size(transmission,2)>1
    transmission=mean(transmission,2);
end


%% Initialize all the global variables

global wnAxis_glob;
wnAxis_glob = firstPeakWn:wnSpacing:firstPeakWn+wnSpacing*(length(transmission)-1);
if reversed==1
    wnAxis_glob=fliplr(wnAxis_glob);
end
global transmission_glob;
transmission_glob= transmission;
global deltaWn;
deltaWn = wnSpacing;
global stop;
stop=false;
global reversed_glob
reversed_glob = reversed;

%% Initialization of global variables of the calibration
calibData = load(evalParams.calibReferencePath);

if (exist('calibData', 'var') == 0)
    error('Error while loading data from calibReferencePath')
end

global calibTR_glob
calibTR_glob = calibData.calibTr;
global calibWn_glob
calibWn_glob = calibData.calibWn;



%% Callback function to recover the keystrokes
function key_stroke_function(fig_obj,eventDat)

    if contains(eventDat.Key,'leftarrow')
        wnAxis_glob=wnAxis_glob-deltaWn;
        update(fig_obj);
    elseif contains(eventDat.Key,'rightarrow')
        wnAxis_glob = wnAxis_glob+deltaWn;
        update(fig_obj);
    elseif contains(eventDat.Key,'uparrow') || contains(eventDat.Key,'downarrow')
        %Flip the axis
        wnAxis_glob =fliplr(wnAxis_glob); 
        %Change the reverse variable
        reversed_glob = ~reversed_glob;
        update(fig_obj);
    elseif contains(eventDat.Key,'escape')
        stop=true;
    end
end

%% Function to update the figure
function update(fig_obj)
    
    h=findobj(fig_obj,'Type','line');
    if ~isempty(h)
        delete(h);
    end
    plot(calibWn_glob,calibTR_glob,'b')
    plot(wnAxis_glob,transmission_glob,'r')
    xlim([min(wnAxis_glob)-50 max(wnAxis_glob)+50])
%     ylim([min(transmission_glob) max(transmission_glob)])
    ylim([0,1])
    xlabel('Wavenumber cm^-1')
    ylabel('Transmission')
  


end

fig = figure;
set(fig,'KeyPressFcn',@key_stroke_function)
hold on;
% legend('Reference measurement' ,'Sample measurement')
title("Press left/right arrow to shift the measurement - Press up/down arrow to flip it Press ESC to exit")
update(fig)

while ~stop
    pause(0.1)
end


close(fig)

reversed=reversed_glob;
firstPeakWn=min(wnAxis_glob);

evalParams.isReversed = reversed;
evalParams.firstCalibPeakWn = firstPeakWn;
   
clear wnAxis_glob calibTR_glob calibWn_glob stop transmission_glob deltaWn reversed_glob




end
