function [sm, sme, tau] = calcAllanInSec ( timeTrace, sampRate, tauLength, name, plotOn )
% This function calculates the Allan deviation of a time trace and plots
% it. The function adjusts the time axis in the plot properly when you give
% it the appropriate sampling rate of the data.
% Inputs:
% - timeTrace: time domain data.
% - sampRate: rate at which individual data points are taken in timeTrace.
% - tauLength: Number of points in the Allan plot, 40 is a good value.
% - name: Name written above the plot.
% - plotOn: Whether or not to plot the result.
% Outputs:
% - sm: Allan deviation
% - sme: Error of Allan deviation.
% - tau: x-axis values where Allan deviation is calculated.

data.rate = sampRate/10^floor(log10(sampRate));

%Signal channel
data.freq = timeTrace;
tau = logspace(log10(1/data.rate),log10(length(data.freq)*(1/data.rate/2)),tauLength);
tau=tau-mod(tau,1/data.rate);
[sm, ~, sme, tau]=allan_v_2_24(data,tau,name, 0);

tau = tau./10^floor(log10(sampRate));

%% Plot results
if plotOn
    figure
    
    FontName = 'Arial';
    FontSize = 14;
    plotlinewidth=2;
    
    
    g=subplot(2,1,1);
    title(['Allan Deviation: ' name],'FontSize',FontSize+2,'FontName',FontName);
    p = get(g,'position');
    p(4) = p(4)*0.3; % Add 10 percent to height
    p(2) = 0.97-p(4);
    set(g, 'position', p);
    
    xAx=1:length(timeTrace);
    xAx=xAx./sampRate;
    plot(xAx,timeTrace)
    xlabel('time (s)', 'FontSize',FontSize,'FontName',FontName);
    set(gca,'FontSize',FontSize,'FontName',FontName);
    
    g=subplot(2,1,2);
    p = get(g,'position');
    p(4) = p(4)*1.5; % Add 10 percent to height
    p(2) = 0.65-p(4);
    set(g, 'position', p);
    % Choose loglog or semilogx plot here    #PLOTLOG
    %semilogx(tau,sm,'.-b','LineWidth',plotlinewidth,'MarkerSize',24);
    loglog(tau,sm,'.-b','LineWidth',plotlinewidth,'MarkerSize',24);
    
    % in R14SP3, there is a bug that screws up the error bars on a semilog plot.
    %  When this is fixed in a future release, uncomment below to use normal errorbars
    %errorbar(tau,sm,sme,'.-b'); set(gca,'XScale','log');
    % this is a hack to approximate the error bars
    hold on; plot([tau; tau],[sm+sme; sm-sme],'-k','LineWidth',plotlinewidth);
    
    grid on;
    
    %set(get(gca,'Title'),'Interpreter','none');
    xlabel('\tau [sec]','FontSize',FontSize,'FontName',FontName);
    if isfield(data,'units')
        ylabel(['\sigma_y(\tau) [' data.units ']'],'FontSize',FontSize,'FontName',FontName);
    else
        ylabel('\sigma_y(\tau)','FontSize',FontSize,'FontName',FontName);
    end
    set(gca,'FontSize',FontSize,'FontName',FontName);
    % expand the x axis a little bit so that the errors bars look nice
    adax = axis;
    axis([adax(1)*0.9 adax(2)*1.1 adax(3) adax(4)]);
    
    title(['Allan Deviation: ' name],'FontSize',FontSize+2,'FontName',FontName);
end
