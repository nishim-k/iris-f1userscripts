function dataOut=FourierSpectrum(data,dt,plotOn,varargin)
% This function calculates and possibly plots the Fourier spectrum of data.
% Input:
% - data: Timedomain data to transform
% - dt: Time step between datapoints
% - plotOn: wether or not to plot the spectrum
% - varargin: optional parameter containing the name of the time domain
%   data which is then used in the title of the plot.

NFFT = 2^nextpow2(length(data));
Y = fft(data,NFFT);
f = 1/dt/2*linspace(0,1,NFFT/2+1);
timeAxis = dt*linspace(0, length(data), length(data));
Out(:,1) = f;
Out(:,2) = abs(Y(1:NFFT/2+1));
dataOut=Out;

% Plot single-sided amplitude spectrum.
if plotOn
    figure
    subplot(3,1,1)
    plot(timeAxis,data)
    if size(varargin)>0
        title(strcat({'Time trace of '},varargin(1)))
    else
        title('Time trace of signal')
    end
    xlabel('Time [s]')
    
    subplot(3,1,2)
    plot(f,(abs(Y(1:NFFT/2+1))))
    if size(varargin)>0
        title(strcat({'Single-Sided Amplitude Spectrum of '},varargin(1)))
    else
        title('Single-Sided Amplitude Spectrum of Data')
    end
    xlabel('Frequency (Hz)')
    ylabel('Fourier Component |Y(f)|')
    
    subplot(3,1,3)
    plot(f,(angle(Y(1:NFFT/2+1))))
    if size(varargin)>0
        title(strcat({'Phase Spectrum of '},varargin(1)))
    else
        title('Phase Spectrum of Data')
    end
    xlabel('Frequency (Hz)')
    ylabel('Phase angle angle(Y(f))')
end