#Package for the analysis of a multi-heterodyne signal. Written in Visitor pattern. 

#Import all the subpackages in the current namespace
#from .dataclasses import *
#from .visitors import *
#from .processor import Processor

__all__ = ['dataclasses','visitors','tests']
