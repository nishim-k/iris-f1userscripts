# -*- coding: utf-8 -*-
"""
Property of IRsweep AG 
evaluation_example
Created by Raphael Horvath on 03/12/2020.
Copyright (c) 2020 IRsweep. All rights reserved.
"""

from utilities.plotting_utilities import plotting_utilities

#%% Manual wavelength calibration of calibration measurement
# # !!!!! Only execute this section, if you really want to (re)calibrate the calibration measurement !!!!

calibration_name = r'C:\XXXX\XXXX_processed_data.h5'
reference_name = r'C:\XXXX\XXXX.mat'

calibProc = plotting_utilities()
calibProc.load_configuration(calibration_name)
calibProc.load_transmission()
calibProc.calibrateWNaxisOfCalibration(calibFilename=calibration_name, refFilename=reference_name, plotOn=False)


#%% load and process the data

#################################
# enter processing parameters here

# define the measurement
measurement_name = r'C:\XXXX\XXXX_processed_data.h5'
calibrated_calibration_name = r'C:\XXXX\XXXX_calibrated_processed_data.h5'

# select the start and stop for time resolved acquisition averaging; ignored for long term measurements
startIndx = None
stopIndx = None

# choose the spectral halfwidth for boxcar averaging; ignored if Gaussian averaging is used
spectralHalfWidth = 5

# chose whether to use Gaussian averaging and the sigma value for spectral resolution
gaussian_averaging = False
gaussianWNsigma = 0.6

# show the plots of the individual steps
plotOn = False

# option to turn on and off calibration
calibrate = False

# user input ends here
################################


proc = plotting_utilities()
proc.load_proc(measurement_name, calibrated_calibration_name, calibrate)
proc.average(startIndx, stopIndx, spectralHalfWidth, gaussian_averaging, gaussianWNsigma, plotOn)


#%% plot some or all of the acquisitions

#################################
# enter plotting parameters here
# plot specific times 
# in seconds for long term; milliseconds for time resolved
plot_at_time = [0, 0.07, 0.20] 

# use plot_range = True to show all plots in range of plot_at_time 
plot_range = False 

# select a time resolution in milliseconds; time resolved measurement only
time_resolution = 0.004

# plot title and y-axis label and whether to include a legend
title = ''
yaxis = ''
legend = True

# to plot a custom/modified dataset, designate it here
# write None to use the default data, as processed above
custom_input = None

# choose what to plot by selecting 'transmission', 'absorption', 'absorbance' 
plot_type = 'absorbance'

# select a color scheme
color_scheme = 'IRsweep'

# turn plotting off if necessary
plotOn = True

# user input ends here
################################


spectra = proc.acquisition_plotter(plot_at_time, custom_input, time_resolution, title, yaxis, plot_range, legend, plot_type, color_scheme, plotOn)

#%% plot the kinetics at defined points in the spectrum

#################################
# enter plotting parameters here
# select the wavenumber points at which to plot
plot_at_wn = [1260, 1277] 

# plot title and y-axis label 
title = ''
yaxis = ''

# designate an offset to adjust the trigger time 
# seconds for long term; milliseconds for time resolved
offset = 0

# select a time resolution in milliseconds; time resolved measurement only
time_resolution = 0.004

# to plot a custom/modified dataset, designate it here
# write None to use the default data, as processed above
custom_input = None

# choose what to plot by selecting 'transmission', 'absorbance' or 'absorption'
plot_type = 'absorbance'

# select a color scheme
color_scheme = 'IRsweep'

# turn plotting off if necessary
plotOn = True

# user input ends here
################################


kinetics = proc.transient_plotter(plot_at_wn, custom_input, time_resolution, title, yaxis, offset, plot_type, color_scheme, plotOn)

#%% export data
proc.csv_export(spectra)

