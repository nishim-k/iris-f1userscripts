# -*- coding: utf-8 -*-
"""
Property of IRsweep AG 
postProcessor
heterodyne_postprocessing
Created by Quentin Saudan on 10/12/2018.
Copyright (c) 2018 IRsweep. All rights reserved.
"""

import os,sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from heterodyne_postprocessing.processing.postProcessorPlotTransients import PostProcessorPlotTransient


class PostProcessor(PostProcessorPlotTransient):
    def __init__(self):
        super().__init__()