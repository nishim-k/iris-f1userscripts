# -*- coding: utf-8 -*-
"""
Property of IRsweep AG 
postProcessorAvg
heterodyne_postprocessing
Created by Quentin Saudan on 10/12/2018.
Copyright (c) 2018 IRsweep. All rights reserved.
"""

import os,sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from heterodyne_postprocessing.processing.postProcessorConfigurationMethods import PostProcessorConfigurationMethods

import numpy as np
import matplotlib.pyplot as plt


class PostProcessorAvg(PostProcessorConfigurationMethods):
    def __init__(self):
        super().__init__()
        
        
        
    def acquisition_average(self,startIndx=None,stopIndx=None,plotOn=False):
        """
        Function to average acquisitions together. Saves the data under 
        proc.data_name+'AvgOfFiles'.
        
        Input   :   startIndx(int) the first acqusition to take
                    stopIndx(int) the last acquisition to take
                    plotOnt(bool) boolean to plot or not data before/after
        """
        if startIndx is None:
            startIndx = 0
        if stopIndx is None:
            stopIndx = self.data['numAcq']
        else:
            stopIndx = stopIndx+1
            
        tmp_avg = self.averageConfiguration(startIndx, stopIndx)
        
        self.data.update({self.data_name+'AvgOfFiles':tmp_avg})
        self.last_data_type = 'AvgOfFiles'
        
        if plotOn:
            fig,ax = plt.subplots()
            if self.config.processor == 'TimeResolved':
                ax.plot(self.data['timeAxis']*1e3,self.complexToReal(self.data[self.data_name][:,self.config.maxPeakNo,startIndx]),label='First acquisition')
                ax.plot(self.data['timeAxis']*1e3,self.complexToReal(self.data[self.data_name+'AvgOfFiles'][:,self.config.maxPeakNo]),label='Average of '+str(stopIndx-startIndx)+' files')
                ax.set_xlabel('Time [ms]')
                ax.set_ylabel('Transmission')
            elif self.config.processor == 'TimeIntegrated':
                ax.plot(self.data['wnAxis'],self.complexToReal(self.data[self.data_name][:,startIndx]),label='First acquisition')
                ax.plot(self.data['wnAxis'],self.complexToReal(self.data[self.data_name+'AvgOfFiles'][:]),label='Average of '+str(stopIndx-startIndx)+' files')
                ax.set_xlabel('Wavenumber [cm$^{-1}$]')
                ax.set_ylabel('Transmission')    
            ax.legend()
    
    def gaussian(self,x, mu, sig):
        return (1./(np.sqrt(2.*np.pi)*sig)*np.exp(-np.power((x - mu)/sig, 2.)/2))
    
    def spectral_smoothing(self,spectralHalfWidth=None,plotOn=False, gaussianConvolve=False,gaussianWNsigma=None):
        """
        Function to spectrally average the transmission. The smoothing can be done ether by a window average
        with weights corresponding to the inverse of the standard deviation of
        each peaks (i-spectralHalfWidth:i+spectralHalfWidth+1 -> i) if the gaussianConvolve is set to False or by an additional convolution with a gaussian.
        In the later case the spectralHalfwidth is ignored and the gaussianWNsigma has to be defined.
        The data is saved under proc.data_name+'SpectralAvgOfFiles'
        
        Input   :   spectralHalfWidth(int) half of the size of the window to 
                    average points together. Usually set to 5.
                    plotOn(bool) boolean to plot or not the data before/after
                    gaussianConvolve(bool) loolean to add a gaussian concolution on top
                    gaussianWNsigma sets the sigma in cm^-1
                    
        """
        old_avg = np.copy(self.data[self.data_name+'AvgOfFiles'])
        new_avg = np.copy(self.data[self.data_name+'AvgOfFiles'])
       
        
        if gaussianConvolve is False:
            spectralHalfWidth = np.int(spectralHalfWidth)
            for i in np.arange(0,old_avg.shape[-1]):
                start = np.max([0,i-spectralHalfWidth])
                stop = np.min([i+spectralHalfWidth+1,old_avg.shape[-1]])
                if self.config.processor == 'TimeResolved':
                        new_avg[:,i] = self.smoothingAvg(old_avg, start, stop, self.weights(start,stop))
                elif self.config.processor == 'TimeIntegrated':
                        new_avg[i] = self.smoothingAvg(old_avg, start, stop, self.weights(start,stop))
                
                    
        elif gaussianConvolve is True: 
             gaussianWNsigma=abs(gaussianWNsigma/(self.data['wnAxis'][1]-self.data['wnAxis'][0])) #calculate from cm^-1 to line numbers
             spectralHalfWidth = np.int(np.ceil(gaussianWNsigma))*3 #calculates the considered points
             for i in np.arange(0,old_avg.shape[-1]):
                start = np.max([0,i-spectralHalfWidth])
                stop = np.min([i+spectralHalfWidth+1,old_avg.shape[-1]])
                if self.config.processor == 'TimeResolved':
                        new_avg[:,i] = self.smoothingAvg(old_avg, start, stop, self.weightsGaussian(start,stop,i,gaussianWNsigma)) 
                elif self.config.processor == 'TimeIntegrated':
                        new_avg[i] = self.smoothingAvg(old_avg, start, stop, self.weightsGaussian(start,stop,i,gaussianWNsigma))                  
       
                    
                    
        self.data.update({self.data_name+'SpectralAvgOfFiles':new_avg})
        self.last_data_type = 'SpectralAvgOfFiles'
        
        
        if plotOn:
            fig,ax = plt.subplots()
            if self.config.processor == 'TimeResolved':
                ax.plot(self.data['timeAxis']*1e3,self.complexToReal(self.data[self.data_name+'AvgOfFiles'][:,self.config.maxPeakNo]),label='No spectral averaging')
                ax.plot(self.data['timeAxis']*1e3,self.complexToReal(self.data[self.data_name+'SpectralAvgOfFiles'][:,self.config.maxPeakNo]),label='Spectral averaged')
                ax.set_xlabel('Time [ms]')
                ax.set_ylabel('Transmission')
            elif self.config.processor == 'TimeIntegrated':
                ax.plot(self.data['wnAxis'],self.complexToReal(self.data[self.data_name+'AvgOfFiles'][:]),label='No spectral averaging')
                ax.plot(self.data['wnAxis'],self.complexToReal(self.data[self.data_name+'SpectralAvgOfFiles'][:]),label='Spectral averaged')
                ax.set_xlabel('Wavenumber [cm$^{-1}$]')
                ax.set_ylabel('Transmission') 
            ax.legend()
            
    def weights(self,start,stop):
        """
        Function to generate the weights either from stdPeak or from the
        pretrigger slices in a time-resolved measurement.
        
        Input   :   start(int) the first peak where to compute the weight
                    stop(int) the last peak where tocompute the weight
        """
        weight = 1/self.getStdAxis(start,stop)**2
        weight = weight/np.sum(weight)
        return weight
    
    def weightsGaussian(self,start,stop,index,gaussianWNsigma):
        """
        Function to generate the weights either from stdPeak or from the
        pretrigger slices in a time-resolved measurement and covolute with a gaussian distrubution.
        
        Input   :   start(int) the first peak where to compute the weight
                    stop(int) the last peak where tocompute the weight
                    index(int) the index defining the expectation vaule of the gaussian distribution
        """

        norm=[]
        for i in range(stop-start):
            norm.append(self.gaussian(i,index-start,gaussianWNsigma))
            

        weight = 1/self.getStdAxis(start,stop)**2
        weight = weight*norm
        weight = weight/np.sum(weight) 
        return weight
    
    def getStdAxis(self,start=0,stop=None,plotOn=False):
        """
        Function that gives you the standard deviation of a time resolved
        measurement 
        
        """
        
        if self.config.processor == 'TimeIntegrated':
            return self.data['stdPeak'][start:stop]
        else:
            if self.config.useBackgroundIntegrationTime: 
                backgroundSlices = np.int(np.floor(self.config.backgroundIntegrationSamples/self.config.sampleRate/self.config.interleaveTimeStep))
            else:
                backgroundSlices = np.int(np.floor(self.config.preTriggerSamples/self.config.sampleRate/self.config.interleaveTimeStep))
                
            if backgroundSlices < 2:
                raise ValueError("Too short pre trigger time / too few pre trigger samples to calculate the std of heterodyne lines")
        
            if stop is None:
                std = np.std(self.data[self.data_name+'AvgOfFiles'][0:backgroundSlices,start:],axis=0)
            else:
                std = np.std(self.data[self.data_name+'AvgOfFiles'][0:backgroundSlices,start:stop],axis=0)
            return std    
    
    
    def getOutputInType(self,data_in,mode_out,mode_in='transmission'):
        """
        Function that generates a real ouptut in one of the following format : 
            transmission
            absorbance
            absorption
            
        Input   :   data_in(ndarray) an array with data of type mode_in
                    mode_out(str) the type of data to output
                    mode_int(str) the type of data in input
        
        Output  :   data(ndarray) an array of same size as input but with the
                    good physical meaning. Values are real.
        """
        
        #Transform everything to transmission first
        if mode_in == 'transmission':
            data = data_in
        elif mode_in == 'absorption':
            data = 1-data_in
        elif mode_in == 'absorbance':
            data = np.power(10,-data_in)
        else:
            raise RuntimeError('in postProcessorAvg.getOutputInType : input type not recognized.')
            
        
        if mode_out =='transmission':
            data_out = data
        elif mode_out == 'absorption':
            data_out = 1-data
        elif mode_out == 'absorbance':
            data_out = -np.log10(np.abs(data))
        else:
            raise RuntimeError('in postProcessorAvg.getOutputInType : input type not recognized.')
            
        return data_out
    
    
if __name__ == '__main__':
    proc = PostProcessorAvg()
    proc.load_configuration()
    proc.load_transmission()
    proc.acquisition_average(plotOn=True)
    proc.spectral_smoothing(5,plotOn=True)